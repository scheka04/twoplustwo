﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TwoPlusTwo.Common;
using TwoPlusTwo.Model;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace TwoPlusTwo.ViewModel
{
    public class HeaderButtonViewModel : INotifyPropertyChanged
    {

        private CellNumber Cell;
        private int RoofRandomNumber = 50;
        private object locker = new object();

        public event EventHandler RePrintMatrix;
        public event EventHandler ChangeHeaderAndList;

        private CellMatrixViewModel _Matrix;
        public CellMatrixViewModel Matrix
        {
            get { return _Matrix; }
            set
            {
                _Matrix = value;
                _Matrix.CreateAndFillMatrix();
            }
        }

        public HeaderButtonViewModel(CellNumber cell)
        {
            this.Cell = cell;
        }

        public HeaderButtonViewModel(int header)
        {
            this.Cell = new CellNumber();
            Height = 130; // temp 
            Number = header;
        }

        public Color Color
        {
            get { return Cell.Color; }
            set
            {
                if(Cell.Color != value)
                {
                    Cell.Color = value;
                    OnPropertyChanged("Color");
                }
            }
        }

        public int Number
        {
            get { return Cell.Number; }
            set
            {
                if(Cell.Number != value)
                {
                    Cell.Number = value;
                    OnPropertyChanged("Number");
                }
            }
        }

        public double Height
        {
            get { return Cell.Height; }
            set
            {
                if(Cell.Height != value)
                {
                    Cell.Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        //Change this method in the future
        private int GetRandomNumber(int roof)
        {
            return new Random().Next(0, roof);
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private RelayCommand _ChangeHeight;
        public RelayCommand ChangeHeight
        {
            get
            {
                if (_ChangeHeight == null)
                    _ChangeHeight = new RelayCommand(
                        obj =>
                        {
                            HeaderButtonViewModel button = obj as HeaderButtonViewModel;
                            if(button != null)
                            {
                                button.Height += 50;
                            }
                        });

                return _ChangeHeight;
            }
        }

        private void RaiseRePrintMatrix(object sender, EventArgs args)
        {
            Matrix.SelectedCell.Clear();
            Matrix.ResetSelectedCells();
            RePrintMatrix?.Invoke(sender, args);
        }

        private void RaiseChangeHeaderAndList(object sender, EventArgs args)
        {
            ChangeHeaderAndList?.Invoke(sender, args);
        }

        private RelayCommand _ChangeNumber;
        public RelayCommand ChangeNumber
        {
            get
            {
                if (_ChangeNumber == null)
                    _ChangeNumber = new RelayCommand(
                        obj =>
                        {
                            HeaderButtonViewModel button = obj as HeaderButtonViewModel;
                            if (button != null)
                            {
                                RaiseChangeHeaderAndList(null, EventArgs.Empty);
                                button.Number = Matrix.ListOfNumber.Header;
                                Matrix.CreateAndFillMatrix();
                                RaiseRePrintMatrix(null, EventArgs.Empty);
                            }
                        });

                return _ChangeNumber;
            }
        }

        public void CheckSummAndResetSelectCells(object obj, EventArgs args)
        {
            int summ = (int)obj;
            if(summ == Number)
            {
                ScaleCellsAndResetCell();
            }
            else
            {
                Matrix.ResetSelectedCells();
                Matrix.SelectedCell.Clear();
            }
        }

        private async void ScaleCellsAndResetCell()
        {
            var taskList = new List<Task>();
            foreach (var c in Matrix.SelectedCell)
            {
                taskList.Add(ShakeElement(c.Button)); 
            }

            await Task.WhenAll(taskList);
            taskList.Clear();

            Matrix.SelectedCell.Clear();
            Matrix.ResetSummer();
            Matrix.ResetSelectedColor();
        }

        private async Task<bool> ShakeElement(Button button)
        {
            await button.TranslateTo(10, 0, 70, Easing.CubicInOut);
            await button.TranslateTo(-20, 0, 70, Easing.CubicInOut);
            await button.TranslateTo(20, 0, 70, Easing.CubicInOut);
            await button.TranslateTo(10, 0, 70, Easing.CubicInOut);
            await button.TranslateTo(0, 1000, 500, Easing.BounceOut);

            return true;
        }
    }
}
