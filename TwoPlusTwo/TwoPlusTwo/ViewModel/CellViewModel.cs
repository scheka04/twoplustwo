﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using TwoPlusTwo.Common;
using TwoPlusTwo.Model;
using TwoPlusTwo.View;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TwoPlusTwo.ViewModel
{
    public class CellViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public CellNumber Cell { get; private set; }

        private ICommand _ChangeColor;
        public ICommand ChangeColor
        {
            get
            {
                if (_ChangeColor == null)
                    _ChangeColor = new Command(Change);

                return _ChangeColor;
            }
        }

        public Button Button { get; set; }

        public CellViewModel(CellNumber cell)
        {
            this.Cell = cell;
        }

        public double Width
        {
            get { return Cell.Width; }
            set
            {
                if (Cell.Width != value)
                {
                    Cell.Width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        private Thickness _Margin;
        public Thickness Margin
        {
            get { return _Margin; }
            set
            {
                if(_Margin != value)
                {
                    _Margin = value;
                    OnPropertyChanged("Margin");
                }
            }
        }

        public int FontSize
        {
            get { return Cell.FontSize; }
            set
            {
                if(Cell.FontSize != value)
                {
                    Cell.FontSize = value;
                    OnPropertyChanged("FontSize");
                }
            }
        }

        public int Number
        {
            get { return Cell.Number; }
            set
            {
                if(Cell.Number != value)
                {
                    Cell.Number = value;
                    OnPropertyChanged("Number");
                }
            }
        }


        private bool _IsVisible;
        public bool IsVisible
        {
            get { return _IsVisible; }
            set
            {
                if(_IsVisible != value)
                {
                    _IsVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }
        }
        public Color ColorCell
        {
            get { return Cell.Color; }
            set
            {
                if(Cell.Color != value)
                {
                    Cell.Color = value;
                    OnPropertyChanged("ColorCell");
                }
            }
        }

        public double Height
        {
            get { return Cell.Height; }
            set
            {
                if (Cell.Height != value)
                {
                    Cell.Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

      
        private void Change(object obj)
        {
            CellViewModel cell = obj as CellViewModel;
            if (cell != null)
                cell.ColorCell = Color.Pink;
        }
    }
}
