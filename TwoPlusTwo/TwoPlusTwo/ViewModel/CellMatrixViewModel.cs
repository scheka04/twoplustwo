﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using TwoPlusTwo.Common;
using TwoPlusTwo.Helpers;
using TwoPlusTwo.View;
using Xamarin.Forms;

namespace TwoPlusTwo.ViewModel
{
    public class CellMatrixViewModel : INotifyPropertyChanged
    {
        private CellViewModel[,] Matrix;
        private int Summer;

        private int MarginBase  => 2;
        private int FontSizeBase => 20;
        private Color ColorBase = Color.Blue;

        public Color SelectedColor { get; private set; }
        public ILevel Level { get; private set; }
        public ListOfNumber ListOfNumber { get; set; }

        private List<CellViewModel> _SelectedCell;
        public List<CellViewModel> SelectedCell
        {
            get { return _SelectedCell; }
            set
            {
                if(_SelectedCell != value)
                {
                    _SelectedCell = value;
                    OnPropertyChanged("SelectedCell");
                }
            }
        }

        public CellMatrixViewModel(ILevel levelOfHard)
        {
            this.Level = levelOfHard;
            SelectedColor = Color.Red;
            SelectedCell = new List<CellViewModel>();
        }

        public void CreateAndFillMatrix()
        {
            Matrix = Level.CreateMatrix();
            FillMatrixRandomNumber(ListOfNumber.listOfNumber);
        }

        public CellViewModel[] GetColumn(int index)
        {
            return Matrix.GetColumn(index);
        }

        public CellViewModel[] GetRow(int index)
        {
            return Matrix.GetRow(index);
        }

        public CellViewModel GetCellByIndexes(int row, int column)
        {
            return Matrix?[row, column];
        }

        private void FillMatrixRandomNumber(List<int> list)
        {
            for(int i = 0; i < Level.RowCount; i++)
            {
                for(int j = 0; j < Level.ColumnCount; j++)
                {
                    Matrix[i, j].Number = list[i * Level.ColumnCount + j];
                    Matrix[i, j].ColorCell = StaticColors.LightBlack;
                    Matrix[i, j].IsVisible = true;
                    Matrix[i, j].Margin = new Thickness(MarginBase);
                    Matrix[i, j].FontSize = FontSizeBase;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler CheckSelectedSummer;

        private void OnRaiseCheckSelectedSummer(object obj, EventArgs args)
        {
            CheckSelectedSummer?.Invoke(obj, args);
        }

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private RelayCommand _SelectCommand;
        public RelayCommand SelectCommand
        {
            get
            {
                if (_SelectCommand == null)
                    _SelectCommand = new RelayCommand(
                        obj =>
                        {
                            
                            CellViewModel cell = obj as CellViewModel;
                            if (cell != null)
                            {
                                if(!SelectedCell.Contains(cell))
                                {
                                    Summer += cell.Number;
                                    SelectedCell.Add(cell);
                                    SelectedColor = new Color(SelectedColor.R + 0.1, SelectedColor.G + 0.1, SelectedColor.B + 0.1);
                                    cell.Margin = new Thickness(MarginBase + 5);
                                    cell.FontSize -= 5;
                                    cell.ColorCell = SelectedColor;

                                    if (SelectedCell.Count == Level.SelectedCellCount)
                                        OnRaiseCheckSelectedSummer(Summer, EventArgs.Empty);
                                }                                
                            }
                        }//,
                        //obj => SelectedCell.Count != Level.SelectedCellCount
                        );

                return _SelectCommand;
            }
        }

        public void ResetSelectedCells()
        {
            ResetSelectedColor();
            foreach (var cell in SelectedCell)
            {
                cell.ColorCell = ColorBase;
                cell.Margin = new Thickness(MarginBase);
                cell.FontSize = FontSizeBase;
                ResetSummer();
            }
        }

        public void ResetSelectedColor()
        {
            SelectedColor = Color.Red;
        }

        public void ResetSummer()
        {
            Summer = 0;
        }
    }
}
