﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TwoPlusTwo.Model
{
    public class CellNumber
    {
        public int Number { get; set; }
        public Color Color { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public int FontSize { get; set; }
    }
}
