﻿using System;
using System.Collections.Generic;
using System.Text;
using TwoPlusTwo.Model;
using TwoPlusTwo.View;
using TwoPlusTwo.ViewModel;

namespace TwoPlusTwo.Common
{
    public class EasyLevel : ILevel
    {
        public Level Level { get; private set; }
        public int HeaderNumberMin { get; set; }
        public int HeaderNumberMax { get; set; }
        public int RowCount { get; private set; }
        public int ColumnCount { get; private set; }

        public int SelectedCellCount { get; private set; }
        public EasyLevel()
        {
            this.Level = Level.EASY;
            HeaderNumberMin = 20;
            HeaderNumberMax = 30;
            RowCount = 5;
            ColumnCount = 4;
            SelectedCellCount = 3;
        }

        public CellViewModel[,] CreateMatrix()
        {
            CellViewModel[,] matrix = new CellViewModel[RowCount, ColumnCount];
            for(int i = 0; i < RowCount; i++)
            {
                for(int j = 0; j < ColumnCount; j++)
                {
                    matrix[i, j] = new CellViewModel(new CellNumber());
                }
            }

            return matrix;
        }
    }
}
