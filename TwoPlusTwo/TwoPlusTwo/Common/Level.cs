﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoPlusTwo.Common
{
    public enum Level
    {
        EASY,
        NORMAL,
        HARD
    }
}
