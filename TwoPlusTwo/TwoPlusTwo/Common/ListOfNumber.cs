﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwoPlusTwo.Helpers;

namespace TwoPlusTwo.ViewModel
{
    public class ListOfNumber
    {
        private Random rnd;

        public List<int> listOfNumber { get; private set; }
        public int Header { get; set; }
        public int Size { get; private set; }
        public ILevel Level { get; set; }

        public ListOfNumber( ILevel level)
        {
            rnd = new Random();
            Level = level;
            listOfNumber = new List<int>();
            Size = level.RowCount * level.ColumnCount;

            Header = GenerateHeader();
            GenerateList();
        }

        public ListOfNumber()
        {
            listOfNumber = new List<int>();
        }

        public void GenerateList()
        {
            listOfNumber.Clear();

            int tempSumm = 0;
            int tempCout = Level.SelectedCellCount;
            int countNumber = 0;
            int number = 0;
            while (Size > countNumber)
            {

                number = 0;
                if (tempCout != 1)
                {
                    do
                    {
                        number = rnd.Next(1, Header / tempCout);
                    }
                    while (listOfNumber.LastOrDefault() == number);

                    listOfNumber.Add(number);
                    tempCout--;
                    tempSumm += number;
                }
                else
                {
                    number = Header - tempSumm;

                    tempSumm = 0;
                    tempCout = Level.SelectedCellCount;

                    listOfNumber.Add(number);
                }
                countNumber++;
            }

            listOfNumber.Shuffle();
        }

        public void ChangeHeaderAndList(object sender, EventArgs args)
        {
            Header = GenerateHeader();
            GenerateList();
        }

        private int GenerateHeader()
        {
            return rnd.Next(Level.HeaderNumberMin, Level.HeaderNumberMax);
        }
    }
}
