﻿using System;
using System.Collections.Generic;
using System.Text;
using TwoPlusTwo.Common;
using TwoPlusTwo.View;
using TwoPlusTwo.ViewModel;

namespace TwoPlusTwo
{
    public interface ILevel
    {
        int HeaderNumberMin { get; }
        int HeaderNumberMax { get; }
        int RowCount { get; }
        int ColumnCount { get; }

        int SelectedCellCount { get; }
        Level Level { get; }
        CellViewModel[,] CreateMatrix();

    }
}
