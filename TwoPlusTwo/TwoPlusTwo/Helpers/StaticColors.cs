﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TwoPlusTwo.Helpers
{
    public static class StaticColors
    {
        public static Color Orange => Color.FromArgb(255, 85, 0);
        public static Color Black => Color.FromArgb(20, 20, 20);
        public static Color LightBlack => Color.FromArgb(54, 54, 54);
        public static Color White => Color.FromArgb(255, 227, 209);
    }
}
