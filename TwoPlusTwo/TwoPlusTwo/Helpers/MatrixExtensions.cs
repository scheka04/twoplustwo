﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using TwoPlusTwo.ViewModel;

namespace TwoPlusTwo.Helpers
{
    public static class MatrixExtensions
    {
        public static T[] GetRow<T>(this T[,] matrix, int rowIndex)
        {
            int rowLength = matrix.GetLength(1);
            T[] rowVector = new T[rowLength];

            for(int i = 0; i < rowLength; i++)
            {
                rowVector[i] = matrix[rowIndex, i];
            }

            return rowVector;
        }

        public static T[] GetColumn<T>(this T[,] matrix, int columnIndex)
        {
            int colLength = matrix.GetLength(0);
            T[] colVector = new T[colLength];

            for(int i = 0; i < colLength; i++)
            {
                colVector[i] = matrix[i, columnIndex];

            }

            return colVector;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
