﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TwoPlusTwo.Common;
using TwoPlusTwo.Helpers;
using TwoPlusTwo.View;
using TwoPlusTwo.ViewModel;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TwoPlusTwo
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private CellMatrixViewModel Matrix;
        private ListOfNumber listOfNumber;
        private Grid grid;
        private Button header;
        private Button score;
        private StackLayout stack;
        private HeaderButtonViewModel headerViewModel;
        DisplayInfo mainDisplayInfo = DeviceDisplay.MainDisplayInfo;

        private double cellHeight;
        private double cellWidth;
        public MainPage()
        {
            InitializeComponent();

            Matrix = new CellMatrixViewModel(new EasyLevel());
            listOfNumber = new ListOfNumber(Matrix.Level);
            Matrix.ListOfNumber = listOfNumber;
            headerViewModel = new HeaderButtonViewModel(listOfNumber.Header);
            headerViewModel.Matrix = Matrix;

            headerViewModel.ChangeHeaderAndList += listOfNumber.ChangeHeaderAndList;
            headerViewModel.RePrintMatrix += ReWriteGrid;
            Matrix.CheckSelectedSummer += headerViewModel.CheckSummAndResetSelectCells;

            CreateMainView();
            CreateScoreButton();
            SetChildren();
        }

        private void CreateMatrix()
        {
            grid = new Grid()
            { 
                RowSpacing = 0,
                ColumnSpacing = 0
            };

            cellHeight = (mainDisplayInfo.Height / mainDisplayInfo.Density - 140 - headerViewModel.Height) / Matrix.Level.RowCount;
            cellWidth = mainDisplayInfo.Width / mainDisplayInfo.Density / Matrix.Level.ColumnCount;

            for (int i = 0; i < Matrix.Level.RowCount; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition() { Height = cellHeight });
            }

            for(int i = 0; i < Matrix.Level.ColumnCount; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = cellWidth });
            }
           
            FillMatrixOfNumber();
        }

        private void FillMatrixOfNumber()
        {
            for (int i = 0; i < Matrix.Level.RowCount; i++)
            {
                for(int j = 0; j < Matrix.Level.ColumnCount; j++)
                {
                    CellViewModel currentCell = Matrix.GetCellByIndexes(i, j);
                    currentCell.Width = cellWidth;
                    currentCell.Height = cellHeight;

                    Button btn = new Button()
                    {
                        CommandParameter = currentCell,
                        TextColor = Color.White,
                        CornerRadius = 30,
                        FontSize = cellHeight * 0.4
                    };

                    currentCell.Button = btn;

                    Binding bindText = new Binding() { Source = currentCell, Path = "Number" };
                    Binding bindColor = new Binding() { Source = currentCell, Path = "ColorCell" };
                    Binding bindCommand = new Binding() { Source = Matrix, Path = "SelectCommand" };
                    Binding bindVisible = new Binding() { Source = currentCell, Path = "IsVisible", Mode = BindingMode.TwoWay};
                    Binding bindMargin = new Binding() { Source = currentCell, Path = "Margin", Mode = BindingMode.TwoWay };

                    btn.SetBinding(Button.TextProperty, bindText);
                    btn.SetBinding(Button.BackgroundColorProperty, bindColor);
                    btn.SetBinding(Button.CommandProperty, bindCommand);
                    btn.SetBinding(Button.IsVisibleProperty, bindVisible);
                    btn.SetBinding(Button.MarginProperty, bindMargin);

                    btn.SetValue(Grid.RowProperty, i);
                    btn.SetValue(Grid.ColumnProperty, j);

                    grid.Children.Add(btn);

                }
            }
        }

        private void CreateMainView()
        {
            mainLayout.Children.Clear();

            CreateHeaderButton();
            CreateMatrix();
        }

        private void CreateHeaderButton()
        {
            header = new Button()
            {
                BorderWidth = 0,
                Margin = new Thickness(2),
                FontSize = 60,
                CommandParameter = headerViewModel,
                CornerRadius = 40,
                BackgroundColor = StaticColors.Orange,
                TextColor = Color.White
            };

            Binding bindHeight = new Binding() { Source = headerViewModel, Path = "Height" };
            Binding bindNumber = new Binding() { Source = headerViewModel, Path = "Number" };
            Binding bindCommand = new Binding() { Source = headerViewModel, Path = "ChangeNumber" };

            header.SetBinding(Button.HeightRequestProperty, bindHeight);
            header.SetBinding(Button.TextProperty, bindNumber);
            header.SetBinding(Button.CommandProperty, bindCommand);
        }

        private void SetChildren()
        {
            mainLayout.Children.Clear();

            mainLayout.Children.Add(stack);
            mainLayout.Children.Add(grid);
        }

        private void ReWriteGrid(object obj, EventArgs rgs)
        {
            grid.Children.Clear();
            FillMatrixOfNumber();
        }

        private void CreateScoreButton()
        {
            score = new Button
            {
                Text = "2000",
                TextColor = Color.White,
                BackgroundColor = StaticColors.Orange,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(0,0, 10, -5),
                CornerRadius = 20
                
            };

            stack = new StackLayout();
            stack.Children.Add(score);
            stack.Children.Add(header);
        }
    }
}
